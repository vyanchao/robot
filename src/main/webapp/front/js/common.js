
function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
//验证手机号
function checkPhone(phone){
    var strs=phone.replace(/(^\s+)|(\s+$)/g, "");//去除前后的空格
    if(!(/^1[34578]\d{9}$/.test(strs))){
        return false;
    }
    return true;
}