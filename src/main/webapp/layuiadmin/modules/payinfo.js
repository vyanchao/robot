/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;   
    i.render({
        elem: "#manage",
        url: "/jiaofei/",
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "companyName",
            title: "单位名称"
        },
        {
            field: "areaName",
            title: "所在地区"
        },
        {
            field: "companyType",
            title: "单位性质"
        },
        {
            field: "teamName",
            title: "队名"
        },
        {
            field: "createTime",
            title: "创建时间"
        },
        {
            field: "status",
            title: "缴费状态",
            templet: function(res){
                if(res.status == true){
                    return '已缴费';
                }else{
                    return '未缴费';
                }
            }
        },
        {
            field: "memberCount",
            title: "参赛队员",
            templat:function (res) {
                return "(查看)"+res;
            }
        },
        {
            field: "value",
            title: "应交金额"
        },
        {
            field: "修改",
            title: "操作"
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("payinfo", {})
});