/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table,
    n = layui.form;
    t.get("admin/type", function (data) {
        var type;
        var hide;
        type =data.data;
        if(type == '0'){
            hide = true;
        }else {
            hide = false;
        }
        i.render({
            elem: "#manage",
            url: "/team/backend/backend",
            page: true,
            cols: [[{
                type: "checkbox"
            },
                {
                    field: "zz",
                    title: "序号",
                    type:"numbers"
                },
                {
                    field: "location",
                    title: "所在地区"
                },
                {
                    field: "companyType",
                    title: "单位性质"
                },
                {
                    field: "companyName",
                    title: "单位名称"
                },
                {
                    field: "teamName",
                    title: "队伍名称"
                },
                {
                    field: "projectName",
                    title: "参赛项目"
                },{
                    field: "teamType",
                    title: "组别"
                },
                {
                    field: "teamScoreSum",
                    title: "总成绩"
                },
                {
                    field: "leaderCount",
                    title: "查看辅导老师(人数)",
                    templet: function(res){
                        return '<a style="color:blue;cursor:pointer"  lay-event="detail">查看</a>('+res.leaderCount+')';
                    }
                },
                {
                    field: "memberCount",
                    title: "查看队员(人数)",
                    templet: function(res){
                        return '<a style="color:blue;cursor:pointer" lay-event="memberCount">查看</a>('+res.memberCount+')';
                    }
                }
                ,{
                    field: "choose",
                    title: "操作",
                    hide:hide,
                    templet: function(res){
                        if(res.choosed == true) {
                            return '<a style="color:gray;cursor:pointer" >已选拔</a>';
                        }else {
                            return '<a style="color:blue;cursor:pointer"  lay-event="choose">选拔</a>';
                        }
                    }
                }
            ]],
            text: {none: '一条数据也没有^_^'}
        })
    }),
    e("teamList", {})
});

