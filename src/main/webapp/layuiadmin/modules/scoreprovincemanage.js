/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;   
    i.render({
        elem: "#manage",
        url: "/team/score/",
        page: true,
        cols: [[{
            type: "checkbox"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "areaId",
            title: "所在地区"
        },
        {
            field: "companyType",
            title: "单位性质"
        },
        {
            field: "companyName",
            title: "单位名称"
        },      
        {
            field: "teamName",
            title: "队伍名称"
        },
        {
            field: "teamMemberName",
            title: "队员名字"
        },
        {
            field: "teamMemberIdCard",
            title: "身份证号"
        },
        {
            field: "teamType",
            title: "组别"
        },
        {
            field: "ck",
            title: "成绩详情",
            templet: function(res){
            	return '<a style="color:blue;"  url="">查看</a>';
            }
        },
        {
            field: "teamScoreSum",
            title: "总成绩"
        },
        {
            field: "leaderCount",
            title: "查看辅导老师(人数)",
            templet: function(res){
            	return '<a style="color:blue;" url="">查看</a>('+res.leaderCount+')';
            }
        },
        {
            field: "memberCount",
            title: "查看队员(人数)",
            templet: function(res){
            	return '<a style="color:blue;"  url="">查看</a>('+res.memberCount+')';
            }
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("scoreprovincemanage", {})
});