/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;
    i.render({
        elem: "#projectTable",
        url: "/project/list",
        page: true,
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
        	field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "name",
            title: "大赛项目"
        },
        {
            field: "descr",
            title: "项目简介"
        },
        {
            field: "team_member_count",
            title: "队员最多可添加数量"
        },
        // {
        //     field: "projectLevelEnum",
        //     title: "级别"
        // },
        {
            field: "bao_ming_start_time",
            title: "报名开始时间",
            templet:function(d){return layui.util.toDateString(d.bao_ming_start_time*1000, "yyyy-MM-dd HH:mm:ss")}
        },
        {
            field: "bao_ming_end_time",
            title: "报名结束时间",
            templet:function(d){return layui.util.toDateString(d.bao_ming_end_time*1000, "yyyy-MM-dd HH:mm:ss")}
        },
        {
            field: "status",
            title: "是否可见",
            templet: function(res){
                if(res.status == true){
                    return '可见';
                }else{
                    return '不可见';
                }
            }

        },
        {
            field: "project_level_enum",
            title: "级别",
            templet: function(res){
                if(res.project_level_enum == '0'){
                    return '省级';
                }else if (res.project_level_enum == '1') {
                    return '市级';
                }
            }
        },
        {
            field: "area_name",
            title: "区域名"
        },
        {
            field: "修改",
            title: "操作",
            templet: function(res){
                var text;
                var isVisible;
                if(res.status){
                    text = "不可见";
                    isVisible = false;
                }else {
                    text = "可见";
                    isVisible = true;
                }
                return '<a style="color:blue;cursor:pointer" href="/project/isVisible?id='+res.id+'&status='+isVisible+'" lay-event="detail">'+text+'</a> ';
            }
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("project", {})
});