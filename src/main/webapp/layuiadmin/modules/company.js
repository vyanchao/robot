/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;   
    i.render({
        elem: "#COMPANY-manage",
        url: "company/",
        page: true,
        
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "name",
            title: "单位简称"
        },
        {
            field: "zhiZhao",
            title: "营业执照名字"
        },
        {
            field: "location",
            title: "所在地区"
        },
        {
            field: "companyType",
            title: "单位性质",
            templet: function(res){
            	if(res.companyType == 'XUEXIAO'){
            		return '学校'
            	}else if(res.companyType == 'JIGOU'){
            		return '机构'
            	}
                
            }
        },
        {
            field: "invoiceHeader",
            title: "发票抬头"
        },
        {
            field: "invoiceTaxNo",
            title: "发票税号"
        },
        {
            field: "createTime",
            title: "创建时间"
        },
        // {
        //     field: "status",
        //     title: "有效标志",
        //     templet: function(res){
        //     	if(res.status == '1'){
        //     		return '有效'
        //     	}else{
        //     		return '<span style="color:red;">无效</span>'
        //     	}
        //
        //     }
        // },
        {
            field: "contactPerson",
            title: "联系人"
        }
//        ,{
//            field: "modify",
//            title: "修改"
//        }
        ]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("company", {})
});