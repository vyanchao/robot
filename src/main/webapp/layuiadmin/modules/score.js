/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;   
    i.render({
        elem: "#manage",
        url: "team/score/",
        page: true,
        cols: [[{
            type: "checkbox"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },        
        {
            field: "location",
            title: "所在地区"
        },{
            field: "projectName",
            title: "参赛名称"
        },
        {
            field: "companyType",
            title: "单位性质"
        },
        {
            field: "companyName",
            title: "单位名称"
        },
        {
            field: "teamName",
            title: "队伍名称"
        },
        {
            field: "teamType",
            title: "组别"
        },
        {
            field: "ck",
            title: "成绩详情",
            templet: function(res){
            	return '<a style="color:blue;cursor:pointer"  lay-event="result">查看</a>';
            }
        },
        {
            field: "teamScoreSum",
            title: "总成绩"
        },
        //{
           // field: "leaderCount",
            //title: "查看辅导老师(人数)",
            //templet: function(res){
            	//return '<a style="color:blue;" url="">查看</a>('+res.leaderCount+')';
            //}
        //},
        //{
            //field: "memberCount",
            //title: "查看队员(人数)",
            //templet: function(res){
            	//return '<a style="color:blue;"  url="" onclick="teamMember('+ res.teamId +')">查看</a>('+res.memberCount+')';
            //}
        //},
       // {
         //   field: "修改",
       //     title: "操作",
       //     templet: function(res){
      //      	return '<a style="color:blue;"  url="">修改成绩</a>';
     //       }
      //  }
        ]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("score",{})
});
