<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>登入 - 机器人大赛报名</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/login.css" media="all">
</head>
<style>
    body.signin {
        background: #18c8f6;
        background: url("${request.contextPath}/js/login.jpg") no-repeat center fixed;
    }
</style>
<body class="signin">

<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>机器人大赛报名</h2>
            <p style="color:#171515">机器人大赛报名后台管理模板系统</p>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-username"
                       for="LAY-user-login-username"></label>
                <input type="text" name="username" id="LAY-user-login-username" lay-verify="required" placeholder="用户名"
                       class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password"
                       for="LAY-user-login-password"></label>
                <input type="password" name="passwd" id="LAY-user-login-password" lay-verify="required" placeholder="密码"
                       class="layui-input">
            </div>
            <div class="layui-form-item" style="margin-bottom: 20px;">
                <input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
            </div>
            <div class="layui-form-item">
                <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-login-submit" id="login-btn">登
                    入
                </button>
            </div>
        </div>
    </div>

    <div class="layui-trans layadmin-user-login-footer">

        <p style="color:#171515;padding-bottom:10px">© 2019 河南领越科技</p>
    </div>

</div>

<script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
<script>
    layui.config({
        base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'user'], function () {
        var $ = layui.$
            , setter = layui.setter
            , admin = layui.admin
            , form = layui.form
            , router = layui.router()
            , search = router.search;

        form.render();


        document.onkeydown = function (e) { // 回车提交表单
            var theEvent = window.event || e;
            var code = theEvent.keyCode || theEvent.which;
            if (code == 13) {
                $("#login-btn").click(); // #login-btn 是你 提交按钮的ID
            }
        }

        //提交
        form.on('submit(LAY-user-login-submit)', function (obj) {

            //请求登入接口
            admin.req({
                url: '/admin/login'
                , contentType: 'application/json;charset=utf-8'
                , data: JSON.stringify(obj.field)
                , type: 'post'
                , success: function (result, status) {
                    if (result.success == true) {
                        //登入成功的提示与跳转
                        //layer.msg('登入成功', {
                        //icon:1
                        //,time: 1000
                        //,shade: 0.3
                        //}, function(){
                        //});
                        location.href = '/index'; //后台主页
                    } else {
                        layer.msg('登陆失败', {
                            icon: 1
                            , time: 1000
                            , shade: 0.3
                        });
                    }
                }
                , error: function () {
                    layer.msg('登陆出错', {
                        icon: 1
                        , time: 1000
                        , shade: 0.3
                    });
                }
            });
        });
    });
</script>
</body>
</html>