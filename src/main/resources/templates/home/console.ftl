

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layuiAdmin 控制台主页一</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="../../layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="../../layuiadmin/style/admin.css" media="all">
</head>
<body>
  
  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md8">
        <div class="layui-row layui-col-space15">
          <div class="layui-col-md6">
            <div class="layui-card">
              <div class="layui-card-header">快捷方式</div>
              <div class="layui-card-body">
                
                <div class="layui-carousel layadmin-carousel layadmin-shortcut">
                  <div carousel-item>
                    <ul class="layui-row layui-col-space10">
                      <li class="layui-col-xs3">
                        <a lay-href="companyList">
                          <i class="layui-icon layui-icon-component"></i>
                          <cite>单位列表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="teamManage">
                          <i class="layui-icon layui-icon-username"></i>
                          <cite>报名队员列表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a lay-href="ranksManage">
                          <i class="layui-icon layui-icon-group"></i>
                          <cite>报名队伍列表</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3">
                        <a layadmin-event="scoreCityManage">
                          <i class="layui-icon layui-icon-list"></i>
                          <cite>成绩管理</cite>
                        </a>
                      </li>
                      <li class="layui-col-xs3" >
                        <a lay-href="teamManagement">
                          <i class="layui-icon layui-icon-set-fill"></i>
                          <cite>管理员列表</cite>
                        </a>
                      </li>
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="app/workorder/list.html">-->
                          <#--<i class="layui-icon layui-icon-survey"></i>-->
                          <#--<cite>工单</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="user/user/list.html">-->
                          <#--<i class="layui-icon layui-icon-user"></i>-->
                          <#--<cite>用户</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/system/website.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>设置</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                    </ul>
                    <#--<ul class="layui-row layui-col-space10">-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                      <#--<li class="layui-col-xs3">-->
                        <#--<a lay-href="set/user/info.html">-->
                          <#--<i class="layui-icon layui-icon-set"></i>-->
                          <#--<cite>我的资料</cite>-->
                        <#--</a>-->
                      <#--</li>-->
                    <#--</ul>-->
                    
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          <div class="layui-col-md6">
            <div class="layui-card">
              <div class="layui-card-header">统计</div>
              <div class="layui-card-body">

                <div class="layui-carousel layadmin-carousel layadmin-backlog">
                  <div carousel-item>
                    <ul class="layui-row layui-col-space10">
                      <li class="layui-col-xs6">
                        <a href="javascript:;" class="layadmin-backlog-body">
                          <h3>参赛学校数量</h3>
                          <p><cite>${schoolCount}</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a href="javascript:;" class="layadmin-backlog-body">
                          <h3>参赛机构数量</h3>
                          <p><cite>${jigouCount}</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a href="javascript:;" class="layadmin-backlog-body">
                          <h3>参赛队伍数</h3>
                          <p><cite>${teamCount}</cite></p>
                        </a>
                      </li>
                      <li class="layui-col-xs6">
                        <a href="javascript:;" onclick="layer.tips('从菜单中看详情', this, {tips: 3});" class="layadmin-backlog-body">
                          <h3>参赛队员数</h3>
                          <p><cite>${memberCount}</cite></p>
                        </a>
                      </li>
                    </ul>
                    <#--<ul class="layui-row layui-col-space10">-->
                      <#--<li class="layui-col-xs6">-->
                        <#--<a href="javascript:;" class="layadmin-backlog-body">-->
                          <#--<h3>待审友情链接</h3>-->
                          <#--<p><cite style="color: #FF5722;">5</cite></p>-->
                        <#--</a>-->
                      <#--</li>-->
                    <#--</ul>-->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <#--<div class="layui-col-md12">-->
            <#--<div class="layui-card">-->
              <#--<div class="layui-card-header">数据概览</div>-->
              <#--<div class="layui-card-body">-->

                <#--<div class="layui-carousel layadmin-carousel layadmin-dataview" data-anim="fade" lay-filter="LAY-index-dataview">-->
                  <#--<div carousel-item id="LAY-index-dataview">-->
                    <#--<div><i class="layui-icon layui-icon-loading1 layadmin-loading"></i></div>-->
                    <#--<div></div>-->
                    <#--<div></div>-->
                  <#--</div>-->
                <#--</div>-->

              <#--</div>-->
            <#--</div>-->

          <#--</div>-->
        </div>
      </div>
      
      <div class="layui-col-md4">
        <div class="layui-card">
          <div class="layui-card-header">系统信息</div>
          <div class="layui-card-body layui-text">
            <table class="layui-table">
              <colgroup>
                <col width="100">
                <col>
              </colgroup>
              <tbody>
                <tr>
                  <td>系统名称</td>
                  <td>
                    <script type="text/html" template>
                      河南省青少年机器人竞赛报名系统
                      <#--<a href="http://fly.layui.com/docs/3/" target="_blank" style="padding-left: 15px;">更新日志</a>-->
                    </script>
                  </td>
                </tr>
                <tr>
                  <td>当前版本</td>
                  <td>
                    <script type="text/html" template>
                      v1.0.1
                    </script>
                 </td>
                </tr>
                <tr>
                  <td>技术支持</td>
                  <td>
                    领越科教
                    <#--windows,java,maven,mysql,layui,mui,nginx,tomcat,js-->
                  </td>
                </tr>
                <tr>
                  <td>所属单位</td>
                  <td style="padding-bottom: 0;">
                    <div class="layui-btn-container">
                      <#--<a href="" target="_blank" class="layui-btn layui-btn-danger">获取授权</a>-->
                      <#--<a href="" target="_blank" class="layui-btn">立即下载</a>-->
                      <a href="http://www.lingyue.net/" target="_blank" class="layui-btn">领越科教 - 河南省青少年机器人竞赛协办单位</a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <#--<div class="layui-card">-->
          <#--<div class="layui-card-header">效果报告</div>-->
          <#--<div class="layui-card-body layadmin-takerates">-->
            <#--<div class="layui-progress" lay-showPercent="yes">-->
              <#--<h3>转化率（日同比 28% <span class="layui-edge layui-edge-top" lay-tips="增长" lay-offset="-15"></span>）</h3>-->
              <#--<div class="layui-progress-bar" lay-percent="65%"></div>-->
            <#--</div>-->
            <#--<div class="layui-progress" lay-showPercent="yes">-->
              <#--<h3>签到率（日同比 11% <span class="layui-edge layui-edge-bottom" lay-tips="下降" lay-offset="-15"></span>）</h3>-->
              <#--<div class="layui-progress-bar" lay-percent="32%"></div>-->
            <#--</div>-->
          <#--</div>-->
        <#--</div>-->
        
        <#--<div class="layui-card">-->
          <#--<div class="layui-card-header">实时监控</div>-->
          <#--<div class="layui-card-body layadmin-takerates">-->
            <#--<div class="layui-progress" lay-showPercent="yes">-->
              <#--<h3>CPU使用率</h3>-->
              <#--<div class="layui-progress-bar" lay-percent="58%"></div>-->
            <#--</div>-->
            <#--<div class="layui-progress" lay-showPercent="yes">-->
              <#--<h3>内存占用率</h3>-->
              <#--<div class="layui-progress-bar layui-bg-red" lay-percent="90%"></div>-->
            <#--</div>-->
          <#--</div>-->
        <#--</div>-->
        
        

        
      </div>
    </div>
  </div>

  <script src="../../layuiadmin/layui/layui.js?t=1"></script>  
  <script>
  layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'console']);
  </script>
</body>
</html>

