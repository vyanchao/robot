<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>河南省青少年机器人竞赛报名系统</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body class="layui-layout-body">
  
  <div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
      <div class="layui-header">
        <!-- 头部区域 -->
        <ul class="layui-nav layui-layout-left">
          <li class="layui-nav-item layadmin-flexible" lay-unselect>
            <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
              <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="" target="_blank" title="前台">
              <i class="layui-icon layui-icon-website"></i>
            </a>
          </li>
          <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;" layadmin-event="refresh" title="刷新">
              <i class="layui-icon layui-icon-refresh-3"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <#--<input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search" layadmin-event="serach" lay-action="template/search.html?keywords="> -->
          </li>
        </ul>
        <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
          
          <!--<li class="layui-nav-item" lay-unselect>
            <a lay-href="app/message/index.html" layadmin-event="message" lay-text="消息中心">
              <i class="layui-icon layui-icon-notice"></i>  
              
               如果有新消息，则显示小圆点 -->
              <!--<span class="layui-badge-dot"></span>
            </a>
          </li>-->
          <!--<li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="theme">
              <i class="layui-icon layui-icon-theme"></i>
            </a>
          </li>-->
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="note">
              <i class="layui-icon layui-icon-note"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="fullscreen">
              <i class="layui-icon layui-icon-screen-full"></i>
            </a>
          </li>
          <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;">
              <cite>${admin.username}</cite>
            </a>
            <dl class="layui-nav-child">
              <#--<dd><a lay-href="set/user/info.html">基本资料</a></dd>-->
              <dd><a lay-href="password">修改密码</a></dd>
              <hr>
              <dd layadmin-event="logout" style="text-align: center;"><a onclick="logout()">退出</a></dd>
            </dl>
          </li>
          
          <li class="layui-nav-item" lay-unselect>
            <i class="layui-icon layui-icon-more-vertical"></i>
          </li>
          
        </ul>
      </div>
      
      <!-- 侧边菜单 -->
      <div class="layui-side layui-side-menu">
        <div class="layui-side-scroll">
          <div class="layui-logo" lay-href="console">
            <span>河南省青少年机器人竞赛报名系统</span>
          </div>
          
          <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
            <li data-name="home" class="layui-nav-item layui-nav-itemed">
              <a href="javascript:;" lay-tips="首页" lay-direction="2">
                <i class="layui-icon layui-icon-home"></i>
                <cite>首页</cite>
              </a>
            </li>
            <li data-name="component" class="layui-nav-item  layui-nav-itemed">
              <a href="javascript:;" lay-tips="单位管理" lay-direction="2">
                <i class="layui-icon layui-icon-component"></i>
                <cite>单位管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="console">
                  <a lay-href="companyList">单位列表</a>
                </dd>
              </dl>
            </li>
            
            <li data-name="component" class="layui-nav-item  layui-nav-itemed">
              <a href="javascript:;" lay-tips="参赛项目管理" lay-direction="2">
                <i class="layui-icon layui-icon-component"></i>
                <cite>参赛项目管理</cite>
              </a>
              <dl class="layui-nav-child">

                <#if admin.adminTypeEnum == "PROVINCEADMIN">
                  <dd data-name="console">
                    <a lay-href="addProvinceProjectInfo">添加项目(省级)</a>
                  </dd>
                </#if>

                <#--<dd data-name="console">-->
                  <#--<a lay-href="addCityProject">添加项目(市级)</a>-->
                <#--</dd>-->
                <dd data-name="console">
                  <a lay-href="projectList">参赛项目列表</a>
                </dd>
              </dl>
            </li>
            
            <li data-name="component" class="layui-nav-item  layui-nav-itemed">
              <a href="javascript:;" lay-tips="已报名/缴费信息管理" lay-direction="2">
                <i class="layui-icon layui-icon-component"></i>
                <cite>已报名/缴费信息管理</cite>
              </a>
              <dl class="layui-nav-child">
                <#if admin.adminTypeEnum == "PROVINCEADMIN">
                <dd data-name="console">
                  <a lay-href="payInfoList">缴费管理(省级)</a>
                </dd>
                </#if>
                <#if admin.adminTypeEnum == "CITYADMIN">
                  <dd data-name="console">
                    <a lay-href="signUpInfoList">已报名信息(市级)</a>
                  </dd>
                </#if>

              </dl>
            </li>
            
            <li data-name="component" class="layui-nav-item  layui-nav-itemed">
              <a href="javascript:;" lay-tips="参赛队伍/队员/成绩管理" lay-direction="2">
                <i class="layui-icon layui-icon-user"></i>
                <cite>参赛队伍/队员/成绩管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="console">
                  <a lay-href="teamManage">报名队员列表</a>
                </dd>
                <dd data-name="console">
                  <a lay-href="ranksManage">报名队伍列表</a>
                </dd>
                <dd data-name="console">
                  <a lay-href="scoreCityManage">成绩管理</a>
                </dd>
              </dl>
            </li>

            <#if admin.superAdmin == true>
            <li data-name="component" class="layui-nav-item  layui-nav-itemed">
              <a href="javascript:;" lay-tips="管理员管理" lay-direction="2">
                <i class="layui-icon layui-icon-set-fill"></i>
                <cite>管理员管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="console">
                  <a lay-href="teamManagement">管理员列表</a>
                </dd>
              </dl>
            </li>  
            </#if>
          </ul>
        </div>
      </div>

      <!-- 页面标签 -->
      <div class="layadmin-pagetabs" id="LAY_app_tabs">
        <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-down">
          <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
            <li class="layui-nav-item" lay-unselect>
              <a href="javascript:;"></a>
              <dl class="layui-nav-child layui-anim-fadein">
                <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
              </dl>
            </li>
          </ul>
        </div>
        <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
          <ul class="layui-tab-title" id="LAY_app_tabsheader">
            <li lay-id="console" lay-attr="console" class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
          </ul>
        </div>
      </div>
      
      
      <!-- 主体内容 -->
      <div class="layui-body" id="LAY_app_body">
        <div class="layadmin-tabsbody-item layui-show">
          <iframe src="console" frameborder="0" class="layadmin-iframe"></iframe>
        </div>
      </div>
      
      <!-- 辅助元素，一般用于移动设备下遮罩 -->
      <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
  </div>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use('index');
  </script>
  <script>
  	function logout(){
  		location.href="logout";
  	}
  </script>
</body>
</html>