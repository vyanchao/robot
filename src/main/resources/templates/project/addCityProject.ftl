<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>添加单位</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
		<link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
	</head>

	<body>
		<form class="layui-form">
			<div class="layui-fluid">
				<div class="layui-row layui-col-space15">
					<div class="layui-col-md12">
						<div class="layui-card">
							<div class="layui-card-header">添加市级项目</div>
							<div class="layui-form-item">
							</div>
							<div class="layui-form" lay-filter="">
								<div class="layui-form-item">
									<label class="layui-form-label">参赛项目：</label>
									<div class="layui-input-inline">
                                        <select name="parentId" id="selectid"  lay-filter="required">
                                            <option value="" selected>请选择参赛项目</option>
                                        </select>
								</div>
								</div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">组别：</label>
                                    <div class="layui-input-outline">
                                        <input type="checkbox" name="projectClassEnum" value="XIAOXUE" title="小学">
                                        <input type="checkbox" name="projectClassEnum" value="CHUZHONG" title="初中">
                                        <input type="checkbox" name="projectClassEnum" value="GAOZHONG" title="高中" >
                                    </div>
                                </div>
                                <div class="layui-form-item">
								<label class="layui-form-label">是否可见：</label>
                                    <div class="layui-input-inline">
                                        <input type="radio" name="status" value="true" title="可见" checked>
                                        <input type="radio" name="status" value="false" title="隐藏">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                        <label class="layui-form-label">报名开始时间：</label>
                                        <div class="layui-input-inline">
                                            <input type="text" name="baoMingStartTime" class="layui-input" id="test-laydate-type-datetime1" placeholder="报名开始时间">
                                        </div>
                                </div>
                                <div class="layui-form-item">
                                        <label class="layui-form-label">报名结束时间：</label>
                                        <div class="layui-input-inline">
                                            <input type="text" name="baoMingEndTime" class="layui-input" id="test-laydate-type-datetime2" placeholder="报名结束时间">
                                        </div>
                                </div>
                                <div class="layui-form-item">
                                        <label class="layui-form-label">项目结束时间：</label>
                                        <div class="layui-input-inline">
                                            <input type="text" name="projectEndTime" class="layui-input" id="test-laydate-type-datetime3" placeholder="项目结束时间">
                                        </div>
                                </div>
                            </div>
						</div>
							<div class="layui-form-item">
								<div class="layui-input-block" style="padding-bottom: 10px;">
									<button class="layui-btn" lay-submit lay-filter="addSubmit">提交</button>
									<input type="button" class="layui-btn layui-btn-primary" onclick="javascript:history.back(-1);" value="返回"></input>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

		<script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
		<script>
			layui.config({
					base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
				}).extend({
					index: 'lib/index' //主入口模块
					,address : 'address'
				}).use(['index', 'set', 'form', 'jquery','laydate'], function() {
					var form = layui.form;
					var $ = layui.$;
					var laydate= layui.laydate;
                $.ajax({
                    url:'/project/queryProject',
                    dataType:'json',
                    type: 'get',
                    success:function(data){
                        $.each(data.data, function (index, project) {
                            var id = project.id;
                            var name = project.name;
                            $("#selectid").append("<option value='"+id+"'>"+name+"</option>");
                            //form.render()渲染将option添加进去
                            form.render();
                        });
                    }
                });
                //日期时间选择器1
                laydate.render({
                    elem: '#test-laydate-type-datetime1',
                    type: 'datetime'
                });

                //日期时间选择器2
                laydate.render({
                    elem: '#test-laydate-type-datetime2',
                    type: 'datetime'
                });

                //日期时间选择器3
                laydate.render({
                    elem: '#test-laydate-type-datetime3',
                    type: 'datetime'
                });
					form.on('submit(addSubmit)', function(data) {
                        var arr = new Array();
                        $("input:checkbox[name='projectClassEnum']:checked").each(function(i){
                            arr[i] = $(this).val();
                        });
                        data.field.projectClassEnum =arr;
						$.ajax({
							type: 'post',
							url: '/project/addCityProject',
							contentType: 'application/json;charset=utf-8',
							data: JSON.stringify(data.field),
							dataType: 'json',
							success: function(result, status, xhr) {
								layer.msg('添加成功', {icon: 1,offset: '200px',shade: 0.3,time: 1000}, function(){
					              location.href="projectList";
					            });
								
							}
						});
						return false;
					});
				});
		</script>

	</body>

</html>