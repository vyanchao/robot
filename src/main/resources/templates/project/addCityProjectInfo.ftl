<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加项目(市级)</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-header">添加项目(市级)</div>
			<div class="layui-form-item">
             </div>
            <div class="layui-form" lay-filter="">
              <div class="layui-form-item">
                <label class="layui-form-label">选择参赛项目：</label>
		          <div class="layui-input-block">
		            <table class="layui-table" lay-even="" lay-skin="" style="width:70%">
		            <colgroup>
		              <col width="150">
		              <col width="150">
		              <col width="200">
		            </colgroup>
		            <tbody>
		              <#list projectList as p>	
		              	<tr>
		                <td>
		                	<input type="checkbox" name="role2${p_index}" title="${p.name}" lay-skin="primary" >
		                	<i class="layui-icon" id="mytest${p_index}" style="float:right"></i>
		                </td>
		                <td>
		                  <input type="checkbox" name="" title="小学" lay-skin="primary" checked>
			              <input type="checkbox" name="" title="初中" lay-skin="primary">
			              <input type="checkbox" name="" title="高中" lay-skin="primary">
			              
		                </td>
		                <td >
		                	是否可见：
		                	<input type="radio" name="role${p_index}" value="" title="可见"  checked>
              				<input type="radio" name="role${p_index}" value="" title="隐藏">
		                </td>
		              </tr>		
		               <script language="javascript">
						 layui.config({
						    base: '${request.contextPath}/layuiadmin/'
						}).use(['layer'],
						function() {
						    $ = layui.jquery;
						
						    var tip_index = 0;
						    $(document).on('mouseenter', '#mytest${p_index}',
						    function() {
						        tip_index = layer.tips('${p.descr}', '#mytest${p_index}', {
						            tips: [3, '#252670'],
						            time: 0
						        });
						    }).on('mouseleave', '#mytest${p_index}',
						    function() {
						        layer.close(tip_index);
						    });						
						});
						</script>             			  
					  </#list>
		            </tbody>
		          </table>
		          </div>
              </div>
              <div class="layui-form-item">
                  <div class="layui-inline">
	              <label class="layui-form-label">报名开始时间：</label>
	              <div class="layui-input-inline">
	                <input type="text" class="layui-input" id="test-laydate-type-datetime1" placeholder="报名开始时间">
	              </div>
	              </div>
	            </div>
                <div class="layui-form-item">
                  <div class="layui-inline">
	              <label class="layui-form-label">报名结束时间：</label>
	              <div class="layui-input-inline">
	                <input type="text" class="layui-input" id="test-laydate-type-datetime2" placeholder="报名结束时间">
	              </div>
	              </div>
	            </div>
                <div class="layui-form-item">
                  <div class="layui-inline">
	              <label class="layui-form-label">项目结束时间：</label>
	              <div class="layui-input-inline">
	                <input type="text" class="layui-input" id="test-laydate-type-datetime3" placeholder="项目结束时间">
	              </div>
	              </div>
	            </div>
	           </div> 
              <div class="layui-form-item">
                <div class="layui-input-block" style="padding-bottom: 10px;">
                  <button class="layui-btn" lay-submit lay-filter="setmyinfo">确认添加</button>
                </div>
              </div>
            </div>         
          </div>
        </div>
      </div>
    </div>
  </div>

  
  <script>
	  layui.config({
	    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
	}).extend({
	    index: 'lib/index' //主入口模块
	}).use(['index', 'set']).use(['index', 'laydate'],
	function() {
	    var laydate = layui.laydate;
	
	    //日期时间选择器1
	    laydate.render({
	        elem: '#test-laydate-type-datetime1',
	        type: 'datetime'
	    });
	
	    //日期时间选择器2
	    laydate.render({
	        elem: '#test-laydate-type-datetime2',
	        type: 'datetime'
	    });
	
	    //日期时间选择器3
	    laydate.render({
	        elem: '#test-laydate-type-datetime3',
	        type: 'datetime'
	    });

        var form = layui.form
                ,address = layui.address();
        var $ = layui.$;
        form.on('submit(addSubmit)', function(data) {
            $.ajax({
                type: 'post',
                url: '/company/',
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify(data.field),
                dataType: 'json',
                success: function(result, status, xhr) {
                    layer.msg('添加成功', {icon: 1,offset: '200px',shade: 0.3,time: 1000}, function(){
                        location.href="companyList";
                    });

                }
            });
            return false;
        });
	});
  </script>
</body>
</html>