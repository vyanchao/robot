<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加项目(省级)</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-header">添加项目(省级)</div>
			<div class="layui-form-item">
             </div>
            <div class="layui-form" lay-filter="">
              <div class="layui-form-item">
                <label class="layui-form-label">项目名称：</label>
                <div class="layui-input-inline">
                  <input type="text" name="name" value="" class="layui-input" style="width: 300px" placeholder="项目名称">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">规则说明：</label>
                <div class="layui-input-inline">
                  <textarea name="descr" placeholder="" class="layui-textarea" style="width: 400px"></textarea>
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">组别：</label>
                <div class="layui-input-outline">
	              <input type="checkbox" name="projectClassEnum" value="XIAOXUE" title="小学">
	              <input type="checkbox" name="projectClassEnum" value="CHUZHONG" title="初中">
	              <input type="checkbox" name="projectClassEnum" value="GAOZHONG" title="高中" >
	            </div>
              </div>
              
              <div class="layui-form-item">
                <label class="layui-form-label">参赛队员数量：</label>
                <div class="layui-input-inline">
                  <input name ="teamMemberCount" id="5" data-step="1" data-min="0" data-max="999" data-digit="0" value="0"/>
                </div>
              </div>
                <div class="layui-form-item">
                  <div class="layui-inline">
	              <label class="layui-form-label">报名开始时间：</label>
	              <div class="layui-input-inline">
	                <input name="baoMingStartTime" type="text" class="layui-input" id="test-laydate-type-datetime1" placeholder="报名开始时间">
	              </div>
	              </div>
	            </div>
                <div class="layui-form-item">
                  <div class="layui-inline">
	              <label class="layui-form-label">报名结束时间：</label>
	              <div class="layui-input-inline">
	                <input name="baoMingEndTime" type="text" class="layui-input" id="test-laydate-type-datetime2" placeholder="报名结束时间">
	              </div>
	              </div>
	            </div>
                <div class="layui-form-item">
                  <div class="layui-inline">
	              <label class="layui-form-label">项目结束时间：</label>
	              <div class="layui-input-inline">
	                <input name="projectEndTime" type="text" class="layui-input" id="test-laydate-type-datetime3" placeholder="项目结束时间">
	              </div>
	              </div>
	            </div>
	            <div class="layui-form-item">
                <label class="layui-form-label">是否可见：</label>
                <div class="layui-input-inline">
                  	<input type="radio" name="status" value="true" title="可见"  checked>
              		<input type="radio" name="status" value="false" title="隐藏">
                </div>
              </div> 
	            
	          </div> 
              <div class="layui-form-item">
                <div class="layui-input-block" style="padding-bottom: 10px;">
                  <button class="layui-btn" lay-submit lay-filter="addSubmit">确认添加</button>
                </div>
              </div>
            </div>         
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
  <script type="text/javascript" src="${request.contextPath}/js/num-alignment.js"></script>
  <script src="${request.contextPath}/js/jquery.min.js"></script>
  <script>
	  layui.config({
	    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
	}).extend({
	    index: 'lib/index' //主入口模块
	}).use(['index', 'set']).use(['index', 'laydate'],
	function() {
	    var laydate = layui.laydate;
	
	    //日期时间选择器1
	    laydate.render({
	        elem: '#test-laydate-type-datetime1',
	        type: 'datetime'
	    });
	
	    //日期时间选择器2
	    laydate.render({
	        elem: '#test-laydate-type-datetime2',
	        type: 'datetime'
	    });
	
	    //日期时间选择器3
	    laydate.render({
	        elem: '#test-laydate-type-datetime3',
	        type: 'datetime'
	    });

        form.on('submit(addSubmit)', function(data) {
            $.ajax({
                type: 'post',
                url: '/project/',
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify(data.field),
                dataType: 'json',
                success: function(result, status, xhr) {
                    layer.msg('添加成功', {icon: 1,offset: '200px',shade: 0.3,time: 1000}, function(){
                        location.href="projectList";
                    });

                }
            });
            return false;
        });
	});
  </script>
  
  <script>	
	// 初始化
	alignmentFns.initialize();		
  </script>
</body>
</html>