<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>选项卡组件</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>
  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-body">
            <table class="layui-table">
              <colgroup>
                <col width="150">
                <col width="150">
                <col width="200">
                <col >
                <col>
              </colgroup>
              <thead>           	
              	<tr>
                  <th>姓名</th>
                  <th>性别</th>
                  <th>身份证号</th>
                  <th>衣服尺码</th>
                  <th>学校</th>
                </tr>               
              </thead>
              <tbody>
              	<#list teamMembers as t>	
                <tr>
                  <td>${t.name}</td>
                  <td><#if t.sex == 1>男</#if><#if t.sex == 2>女</#if></td>
                  <td>${t.idCard}</td>
                  <td>${t.size}</td>
                  <td>${t.school}</td>
                </tr>
                </#list>
              </tbody>
            </table>
          </div>
        </div>
      </div>
   <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index']);
  </script>
</body>
</html>