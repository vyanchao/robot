

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>成绩管理(省级)</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-form layui-card-header layuiadmin-card-header-auto">
        <div class="layui-form-item">
          <div class="layui-inline">
				<label class="layui-form-label">单位名称：</label>
				<div class="layui-input-block">
					<input type="text" name="loginname" placeholder="请输入单位名称" autocomplete="off" class="layui-input" style="width:212px">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">所在地区：</label>
				<div class="layui-input-block">
					<select name="provinceId" lay-filter="provinceId" class="province">
						<option value="">请选择所在省</option>
					</select>
				</div>
			</div>
			<div class="layui-inline">
				<select name="cityId" lay-filter="cityId" disabled>
					<option value="">请选择所在市</option>
				</select>
			</div>
			<div class="layui-inline">
				<select name="areaId" lay-filter="areaId" disabled>
					<option value="">请选择所在区</option>
				</select>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">单位性质：</label>
				<div class="layui-inline">
				    <input type="radio" name="role" value="" title="全部" checked>
					<input type="radio" name="role" value="" title="学校" >
					<input type="radio" name="role" value="" title="机构">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">参赛项目：</label>
				<div class="layui-input-block">
					<select name="role1">
						<option value="0">请选择组别</option>
						<option value="1">智能服务员（B1）</option>
						<option value="2">WRO常规赛（B类）</option>
						<option value="3">FLL机器人工程挑战赛（A类）</option>
						<option value="4">vex-iq工程创新赛（B类）</option>
						<option value="5">智能擂台（C2）</option>
					</select>
				</div>
				<#--<div class="layui-input-block">-->
					<#--<input type="text" name="loginname" placeholder="请输入参赛项目" autocomplete="off" class="layui-input" style="width:212px">-->
				<#--</div>-->
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">队伍名称：</label>
				<div class="layui-input-block">
					<input type="text" name="loginname" placeholder="请输入队伍名称" autocomplete="off" class="layui-input" style="width:212px">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">组别：</label>
				<div class="layui-input-block">
					<select name="role1">
						<option value="0">请选择组别</option>
						<option value="1">小学</option>
						<option value="2">初中</option>
						<option value="3">高中</option>
					</select>
				</div>
			</div>		
			<div class="layui-outline">
			<#--<div class="layui-inline">-->
				<#--<label class="layui-form-label">队员姓名：</label>-->
				<#--<div class="layui-input-block">-->
					<#--<input type="text" name="loginname" placeholder="请输入队员姓名" autocomplete="off" class="layui-input" style="width:212px">-->
				<#--</div>-->
			<#--</div>				-->
			
			<div class="layui-inline">
				<label class="layui-form-label">成绩总分：</label>
				<div class="layui-input-inline" style="width:20%">
					<input type="text" class="layui-input"  placeholder="请输入分数" />
				</div>
				<div class="layui-form-mid">分</div>
				<div class="layui-form-mid">
					-
				</div>
				<div class="layui-input-inline" style="width:20%">
					<input type="text" class="layui-input"  placeholder="请输入分数">
				</div>
				<div class="layui-form-mid">分</div>
			</div>
				<#--vyc-->
			<#--<div class="layui-inline">-->
				<#--<label class="layui-form-label">队员衣服尺码：</label>-->
				<#--<div class="layui-input-block">-->
					<#--<select name="role1" >-->
						<#--<option value="">请选择尺码</option>-->
						<#--<option value="XXL">XXL</option>-->
						<#--<option value="M">M</option>-->
						<#--<option value="L">L</option>-->
						<#--<option value="S">S</option>-->
					<#--</select>-->
				<#--</div>-->
			<#--</div>	-->
			</div>	
			<div class="layui-outline">				
			<#--<div class="layui-inline">-->
				<#--<label class="layui-form-label">队员身份证号：</label>-->
				<#--<div class="layui-input-block">-->
					<#--<input type="text" name="loginname" placeholder="请输入队员身份证号" autocomplete="off" class="layui-input" style="width:212px">-->
				<#--</div>-->
			<#--</div>-->
			<div class="layui-inline">
				<label class="layui-form-label">报名时间：</label>
				<div class="layui-input-inline">
					<input type="text" class="layui-input" id="test-laydate-start" placeholder="报名开始时间">
				</div>
				<div class="layui-form-mid">
					-
				</div>
				<div class="layui-input-inline">
					<input type="text" class="layui-input" id="test-laydate-end" placeholder="报名结束时间">
				</div>
			</div>
          <div class="layui-inline">
            <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
          </div>
        </div>
      </div>

      <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
          <button class="layui-btn layuiadmin-btn-list" data-type="updateExcle">导出成绩信息</button>
          <button class="layui-btn layuiadmin-btn-list" data-type="importExcle">导出成绩模板</button>
          <button class="layui-btn layuiadmin-btn-list"  id="test-upload-type1">导入成绩模板</button>
          <button class="layui-btn layui-btn-warm" data-type="add" style="margin-left: 10px;">发布</button>
        </div>
        <table id="manage" lay-filter="manage"></table> 
      </div>
    </div>
  </div>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
    ,address : 'address'
  }).use(['index', 'scoreprovincemanage', 'table', 'upload', 'jquery', 'address'], function(){
    var table = layui.table
    ,form = layui.form
    ,upload = layui.upload
    ,address = layui.address();
    
    //监听搜索
    form.on('submit(search)', function(data){
      var field = data.field;
      
      //执行重载
      table.reload('manage', {
        where: field
      });
    });
    
    var $ = layui.$, active = {
      updateExcle: function(){
        layer.open({
          type: 2
          ,title: '选择需要导出的字段'
          ,content: ['listformT', 'no']
          ,area: ['650px', '400px']
          ,btn: ['导出', '取消']
          ,yes: function(index, layero){
            //点击确认触发 iframe 内容中的按钮提交
            layer.close(index);
            location.href="/downloadScoreFile";
          }
        }); 
      },
      importExcle: function(){
        layer.confirm('是否导出成绩模板？', function(index){
          window.location.href="${request.contextPath}/js/机器人竞赛成绩表.xls";
          layer.close(index);
        });
      }
    }; 

	upload.render({
      elem: '#test-upload-type1'
      ,url: '/importScore'
      ,accept: 'file' //普通文件
      ,exts: 'xls' //xls
      ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
	    layer.load(); //上传loading
	  }
      ,done: function(res, index, upload){
      	layer.closeAll('loading'); //关闭loading
      	layer.msg('上传成功', {
 			icon:1
      		,time: 1000
      		,shade: 0.3
      	});
      }
      ,error: function(index, upload){
	    layer.closeAll('loading'); //关闭loading
	    layer.msg('上传失败，请重新上传', {
 			icon:1
      		,time: 1000
      		,shade: 0.3
      	});
	  }
    });
    $('.layui-btn.layuiadmin-btn-list').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });

  });
  </script>
</body>
</html>
