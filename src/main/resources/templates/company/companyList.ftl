<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>单位管理</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">   
    <div class="layui-card">
      <div class="layui-form layui-card-header layuiadmin-card-header-auto">
        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">单位名称：</label>
            <div class="layui-input-block">
              <input type="text" name="name" placeholder="请输入单位名称" autocomplete="off" class="layui-input">
            </div>
          </div>
          <div class="layui-inline">
            <label class="layui-form-label">所在地区：</label>
            <div class="layui-input-block">
              <select name="provinceId" lay-filter="provinceId" class="provinceId">
                <option value="">请选择所在省</option>
              </select>
            </div>
          </div>
          <div class="layui-inline">
	          <select name="cityId" lay-filter="cityId" disabled>
	            <option value="">请选择所在市</option>
	          </select>
          </div>
          <div class="layui-inline">
	          <select name="areaId" lay-filter="areaId" disabled>
	            <option value="">请选择所在区</option>
	          </select>
          </div>
          <div class="layui-inline" style="padding-left:40px">
              <input type="radio" name="companyType" value="" title="全部" checked>
              <input type="radio" name="companyType" value="XUEXIAO" title="学校">
              <input type="radio" name="companyType" value="JIGOU" title="机构">

          </div>
          
          <div class="layui-inline">
            <button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="COMPANY-search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
          </div>
        </div>
      </div>
      
      <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
          <button class="layui-btn layuiadmin-btn-admin" data-type="add">新增</button>
          <button class="layui-btn layuiadmin-btn-admin" data-type="edit">修改</button>
        </div>
        
        <table id="COMPANY-manage" lay-filter="COMPANY-manage"></table>
      </div>
    </div>
  </div>

 <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index', //主入口模块
    address : 'address'
  }).use(['index', 'company', 'table', 'jquery', 'address'], function(){
    var $ = layui.$
    ,form = layui.form
    ,table = layui.table
    ,address = layui.address();
    
    //监听搜索
    form.on('submit(COMPANY-search)', function(data){
      var field = data.field;
      
      //执行重载
      table.reload('COMPANY-manage', {
        where: field
      });
    });
  
    //事件
    var active = {
      add: function(){
        location.href="addCompanyInfo";
      },
      edit: function(){
        var checkStatus = table.checkStatus('COMPANY-manage');
        var data = checkStatus.data;
        if(data.length == 1){
        	location.href="editCompanyInfo?id="+data[0].id;
        }else{
        	layer.alert("请选择一行数据进行修改！");
        } 
      }
    }  
    $('.layui-btn.layuiadmin-btn-admin').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });
  });
  </script>
</body>
</html>

