<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>修改地市管理员</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>
 <form class="layui-form">
  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-header">修改地市管理员</div>
			<div class="layui-form-item">

             </div>
            <input type="hidden" value="${admin.id?c}" name="id"/>
            <div class="layui-form" lay-filter="">
              <div class="layui-form-item">
                <label class="layui-form-label">账号：</label>
                <div class="layui-input-inline">
                  <input type="text" name="username" value="${admin.username}" class="layui-input" style="width: 300px" placeholder="请输入账号">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">密码：</label>
                <div class="layui-input-inline">
                  <input type="text" name="passwd" value="${admin.passwd}"  class="layui-input" style="width: 300px" placeholder="请输入密码">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">管理员姓名：</label>
                <div class="layui-input-inline">
                  <input type="text" name="realName" value="${admin.realName}"  placeholder="请输入管理员姓名" class="layui-input" style="width: 300px">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">管理员手机号：</label>
                <div class="layui-input-inline">
                  <input type="text" name="phone" value="${admin.phone}" placeholder="请输入管理员手机号" class="layui-input" style="width: 300px">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">管理员权限：</label>
                <div class="layui-input-inline">
                  	<input type="radio" name="adminTypeEnum" value="CITYADMIN" title="市级" <#if admin.adminTypeEnum=='CITYADMIN'>checked</#if>>
              		<input type="radio" name="adminTypeEnum" value="PROVINCEADMIN" title="省级" <#if admin.adminTypeEnum=='PROVINCEADMIN'>checked</#if>>
                </div>
              </div>
              <div class="layui-form-item">
	              <label class="layui-form-label">所在城市：</label>
	                <div class="layui-input-inline">
	                  <select id="provinceId" name="provinceId" lay-filter="provinceId"   class="province">
	                    <option value="" selected>请选择所在省</option>
	                  </select>
	                </div>
	                <div class="layui-input-inline">
	                  <select id="cityId" name="cityId" lay-filter="cityId" disabled>
	                    <option value="" selected>请选择所在市</option>
	                  </select>
	                </div>
	                <div class="layui-input-inline">
	                  <select id="areaId" name="areaId" lay-filter="areaId" disabled>
	                    <option value="" selected>请选择所在区</option>
	                  </select>
	                </div>
	              </div>
	           </div> 
              <div class="layui-form-item">
                <div class="layui-input-block" style="padding-bottom: 10px;">
                  <button class="layui-btn" lay-submit lay-filter="addSubmit">提交</button>
                  <input type="button" class="layui-btn layui-btn-primary" onclick="javascript:history.back(-1);" value="返回"></input>
                </div>
              </div>
            </div>         
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
<script>
	layui.config({
			base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
		}).extend({
			index: 'lib/index' //主入口模块
			,address : 'address'
		}).use(['index','teammanagement', 'set', 'form', 'jquery'], function() {
			var form = layui.form;
        var $ = layui.$;
        $().ready(function () {
                var adminTypeEnum = "${admin.adminTypeEnum ! ''}";
               if(adminTypeEnum == 'CITYADMIN'){
                   $(':radio[name="adminTypeEnum"]').eq(0).attr("checked",true);
               }
                if(adminTypeEnum == 'PROVINCEADMIN'){
                    $(':radio[name="adminTypeEnum"]').eq(1).attr("checked",true);
                }
                var provinceId= "${admin.provinceId ! '' }";
                var cityId= "${admin.cityId ! '' }";
                var areaId= "${admin.areaId ! ''}";
                provinces(provinceId.replace(",",""),cityId.replace(",",""),areaId.replace(",",""));

            });
			form.on('submit(addSubmit)', function(data) {
				$.ajax({
					type: 'post',
					url: '/admin/',
					contentType: 'application/json;charset=utf-8',
					data: JSON.stringify(data.field),
					dataType: 'json',
					success: function(result, status, xhr) {
						if(result.success == false){
							layer.msg('添加失败,'+result.message, {icon: 1,offset: '200px',shade: 0.3,time: 1000});
						}else{
							layer.msg('添加成功', {icon: 1,offset: '200px',shade: 0.3,time: 1000}, function(){
				              location.href="teamManagement";
				            });
						}
					}
				});
				return false;
			});

        function provinces(provinceId,cityId,areaId) {
            //加载省数据
            var proHtml = '',that = this;
            $.get("/dtArea/getJsonDtArea", function (data) {
                var count;
                for (var i = 0; i < data.length; i++) {
                    var isSelected = "";
                    if(data[i].code == provinceId){
                        isSelected = 'selected';
                        count = i;
                    }
                    proHtml += '<option value="' + data[i].code + '" '+isSelected+'>' + data[i].name + '</option>';
                }
                //初始化省数据
                $("select[name=provinceId]").append(proHtml);
                if(cityId != '') {
                    citys(data[count].childs, cityId, areaId);
                }
                form.render();
                form.on('select(provinceId)', function (proData) {
                    $("select[name=areaId]").html('<option value="">请选择所在区</option>');
                    var value = proData.value;
                    if (value > 0) {
                      citys(data[$(this).index() - 1].childs,cityId,areaId);
                    } else {
                        $("select[name=cityId]").attr("disabled", "disabled");
                    }
                });
            })
        };

        //加载市数据
        function citys (citys,cityId,areaId) {
            var cityHtml = '<option value="">请选择所在市</option>',that = this;
            var count;
            for (var i = 0; i < citys.length; i++) {
                var isSelected = "";
                if(citys[i].code == cityId){
                    isSelected = 'selected';
                    count = i;
                }
                cityHtml += '<option value="' + citys[i].code + '"'+isSelected+'>' + citys[i].name + '</option>';
            }
            $("select[name=cityId]").html(cityHtml).removeAttr("disabled");
            if(areaId != "") {
                areas(citys[count].childs, areaId);
            }
            form.render();
            form.on('select(cityId)', function (cityData) {
                var value = cityData.value;
                if (value > 0) {
                    areas(citys[$(this).index() - 1].childs,areaId);
                } else {
                    $("select[name=areaId]").attr("disabled", "disabled");
                }
            });
        }

        //加载县/区数据
        function areas (areas,areaId) {
            var areaHtml = '<option value="">请选择所在区</option>';
            for (var i = 0; i < areas.length; i++) {
                var isSelected = "";
                if(areas[i].code == areaId){
                    isSelected = 'selected';
                }
                areaHtml += '<option value="' + areas[i].code + '"'+isSelected+'>' + areas[i].name + '</option>';
            }
            $("select[name=areaId]").html(areaHtml).removeAttr("disabled");
            form.render();
        }
		});
</script>
</body>
</html>