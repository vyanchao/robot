<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>管理员列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">   
    <div class="layui-card">
      <div class="layui-form layui-card-header layuiadmin-card-header-auto">
        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">用户名：</label>
            <div class="layui-input-block">
              <input type="text" name="username" placeholder="请输入用户名" autocomplete="off" class="layui-input">
            </div>
          </div>
          <div class="layui-inline">
            <label class="layui-form-label">所在地区：</label>
            <div class="layui-input-block">
              <select name="provinceId" lay-filter="provinceId" class="provinceId">
                <option value="">请选择所在省</option>
              </select>
            </div>
          </div>
          <div class="layui-inline">
	          <select name="cityId" lay-filter="cityId" disabled>
	            <option value="">请选择所在市</option>
	          </select>
          </div>
          <div class="layui-inline">
	          <select name="areaId" lay-filter="areaId" disabled>
	            <option value="">请选择所在区</option>
	          </select>
          </div>
          <div class="layui-inline">
            <label class="layui-form-label">管理员权限：</label>
            <div class="layui-input-block">
              <input type="radio" name="adminTypeEnum" value="" title="全部" checked>
              <input type="radio" name="adminTypeEnum" value="CITYADMIN" title="市级" >
              <input type="radio" name="adminTypeEnum" value="PROVINCEADMIN" title="省级">
            </div>
          </div>
          
          <div class="layui-inline">
            <button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="TEAM-manage-search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
          </div>
        </div>
      </div>
      
      <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
          <button class="layui-btn layuiadmin-btn-admin" data-type="add">新增</button>
        </div>
        
        <table id="adminTable" lay-filter="adminTable"></table>
      </div>
    </div>
  </div>

 <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index', //主入口模块
    address : 'address'
  }).use(['index', 'teammanagement', 'table', 'jquery', 'address'], function(){
    var $ = layui.$
    ,form = layui.form
    ,table = layui.table
    ,address = layui.address();
    
    //监听搜索
    form.on('submit(TEAM-manage-search)', function(data){
      var field = data.field;
      
      //执行重载
      table.reload('adminTable', {
        where: field
      });
    });
  
    //事件
    var active = {
      add: function(){
        location.href="addTeamInfo";
      }
    }  
    $('.layui-btn.layuiadmin-btn-admin').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });
  });
  </script>
</body>
</html>

