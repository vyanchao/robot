<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加地市管理员</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>
 <form class="layui-form">
  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card">
          <div class="layui-card-header">添加地市管理员</div>
			<div class="layui-form-item">
                <!--<label class="layui-form-label">头像：</label>
                <div class="layui-input-inline" style="width: 300px">
                  <input name="avatar" lay-verify="required" id="LAY_avatarSrc" readonly placeholder="图片地址" value="http://cdn.layui.com/avatar/168.jpg" class="layui-input" style="width: 300px">
                </div>
                <div class="layui-input-inline layui-btn-container" style="width: auto;">
                  <button type="button" class="layui-btn layui-btn-primary" id="LAY_avatarUpload">
                    <i class="layui-icon">&#xe67c;</i>上传图片
                  </button>
                  <button class="layui-btn layui-btn-primary" layadmin-event="avartatPreview">查看图片</button >
                </div>-->
             </div>
            <div class="layui-form" lay-filter="">
              <div class="layui-form-item">
                <label class="layui-form-label">账号：</label>
                <div class="layui-input-inline">
                  <input type="text" name="username" value="" class="layui-input" style="width: 300px" placeholder="请输入账号">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">密码：</label>
                <div class="layui-input-inline">
                  <input type="text" name="passwd" value=""  class="layui-input" style="width: 300px" placeholder="请输入密码">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">管理员姓名：</label>
                <div class="layui-input-inline">
                  <input type="text" name="realName" value=""  placeholder="请输入管理员姓名" class="layui-input" style="width: 300px">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">管理员手机号：</label>
                <div class="layui-input-inline">
                  <input type="text" name="phone" value="" placeholder="请输入管理员手机号" class="layui-input" style="width: 300px">
                </div>
              </div>
              <div class="layui-form-item">
                <label class="layui-form-label">管理员权限：</label>
                <div class="layui-input-inline">
                  	<input type="radio" name="adminTypeEnum" value="CITYADMIN" title="市级" checked>
              		<input type="radio" name="adminTypeEnum" value="PROVINCEADMIN" title="省级">
                </div>
              </div>
              <div class="layui-form-item">
	              <label class="layui-form-label">所在城市：</label>
	                <div class="layui-input-inline">
	                  <select name="provinceId" lay-filter="provinceId" class="province">
	                    <option value="" selected>请选择所在省</option>
	                  </select>
	                </div>
	                <div class="layui-input-inline">
	                  <select name="cityId" lay-filter="cityId" disabled>
	                    <option value="" selected>请选择所在市</option>
	                  </select>
	                </div>
	                <div class="layui-input-inline">
	                  <select name="areaId" lay-filter="areaId" disabled>
	                    <option value="" selected>请选择所在区</option>
	                  </select>
	                </div>
	              </div>
	           </div> 
              <div class="layui-form-item">
                <div class="layui-input-block" style="padding-bottom: 10px;">
                  <button class="layui-btn" lay-submit lay-filter="addSubmit">提交</button>
                  <input type="button" class="layui-btn layui-btn-primary" onclick="javascript:history.back(-1);" value="返回"></input>
                </div>
              </div>
            </div>         
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
<script>
	layui.config({
			base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
		}).extend({
			index: 'lib/index' //主入口模块
			,address : 'address'
		}).use(['index','teammanagement', 'set', 'form', 'jquery', 'address'], function() {
			var form = layui.form
			,address = layui.address(); 
			var $ = layui.$;
			form.on('submit(addSubmit)', function(data) {
				$.ajax({
					type: 'post',
					url: '/admin/',
					contentType: 'application/json;charset=utf-8',
					data: JSON.stringify(data.field),
					dataType: 'json',
					success: function(result, status, xhr) {
						if(result.success == false){
							layer.msg('添加失败,'+result.message, {icon: 1,offset: '200px',shade: 0.3,time: 1000});
						}else{
							layer.msg('添加成功', {icon: 1,offset: '200px',shade: 0.3,time: 1000}, function(){
				              location.href="teamManagement";
				            });
						}	
					}
				});
				return false;
			});
		});
</script>
</body>
</html>