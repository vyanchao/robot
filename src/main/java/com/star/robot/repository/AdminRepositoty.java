package com.star.robot.repository;

import com.star.robot.entity.Admin;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface AdminRepositoty extends CrudRepository<Admin, Long> , JpaSpecificationExecutor<Admin> {

    public Admin findByUsernameAndPasswd(String username, String passwd);

    public Admin findByUsername(String username);

}
