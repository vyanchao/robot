package com.star.robot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.star.robot.entity.Company;

public interface CompanyRepository extends  JpaSpecificationExecutor,JpaRepository<Company,Long> {
	public Company  findByName(String name);
	public Company findLongById(Long id);


	@Query(value = "select count(*) from Company where company_type =?1 AND city_id like ?2")
	public Long countByType(String type, String codeLike);



}
