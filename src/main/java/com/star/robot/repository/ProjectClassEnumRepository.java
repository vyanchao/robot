package com.star.robot.repository;

import com.star.robot.enums.ProjectClassEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProjectClassEnumRepository extends JpaSpecificationExecutor, JpaRepository<ProjectClassEnum,Long> {


}
