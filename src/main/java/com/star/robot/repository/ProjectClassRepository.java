package com.star.robot.repository;


import com.star.robot.entity.ProjectClass;
import com.star.robot.enums.ProjectClassEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ProjectClassRepository extends JpaSpecificationExecutor, JpaRepository<ProjectClass,Long> {

    public ProjectClass findByProjectClassEnumAndProject_Id(ProjectClassEnum projectClassEnum , Long projectId);

    public List<ProjectClass> findByProject_Id(Long projectId);
}
