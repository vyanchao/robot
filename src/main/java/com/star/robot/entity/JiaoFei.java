package com.star.robot.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * 队伍缴费　（省级）　
 */
@Entity
@Builder
@Data
@Table(name = "jiaoFei")
@AllArgsConstructor
@NoArgsConstructor
public class JiaoFei {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date createTime;

    @Column
    private String tradeNo;

    @Column
    private String prepayId;

    @Column
    private Float value;//缴费金额

    @OneToOne
    private Team team;

    @Column(columnDefinition = "bit(1) null COMMENT \"是否缴费 1缴费　0未交费\"")
    private Boolean status;
}
