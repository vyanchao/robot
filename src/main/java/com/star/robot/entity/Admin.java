package com.star.robot.entity;

import com.star.robot.dto.PageRequestDto;
import com.star.robot.enums.AdminTypeEnum;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;

@Entity
@Table(name = "admin")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Admin extends PageRequestDto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Long id;

    @Column
    private String username;

    @Column
    private String passwd;

    @Column
    private Boolean status=true;

    @Column
    private Long provinceId;

    @Column
    private Long cityId;

    @Column
    private Long areaId;

    @Column
    private AdminTypeEnum adminTypeEnum; //省级查看全数据　市级查市　

    @Column
    private String phone;

    @Column
    private String realName;
    
    private String location;

    @Column(columnDefinition = "bit(1) not null COMMENT \"是否超管\"")
    private Boolean superAdmin;
}
