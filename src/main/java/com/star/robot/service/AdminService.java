package com.star.robot.service;

import com.star.robot.constant.Constant;
import com.star.robot.entity.Admin;
import com.star.robot.enums.AdminTypeEnum;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class AdminService {

    public String getCurrentUsername(HttpServletRequest request){
        Admin currentAdmin = getCurrentAdmin(request);
        return currentAdmin  == null ? null : currentAdmin.getUsername().toString();
    }

    public Admin getCurrentAdmin(HttpServletRequest request){
        Object currentAdmin = request.getSession().getAttribute(Constant.CURRENTADMIN);

        return (Admin)currentAdmin;
    }

    public Boolean isAdmin(HttpServletRequest request){
        return getCurrentAdmin(request) != null;
    }

    public Long getCurrentAdminCityId(HttpServletRequest request){
        Admin admin =  getCurrentAdmin(request);
        if(admin.getAdminTypeEnum() == AdminTypeEnum.PROVINCEADMIN){
            return null;
        }else{
            return admin.getCityId();
        }
    }

    public AdminTypeEnum getCurrentAdminLevel(HttpServletRequest request){
        Admin admin =  getCurrentAdmin(request);
        return admin.getAdminTypeEnum();
    }
}
