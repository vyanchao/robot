package com.star.robot.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.star.robot.service.ScoreService;

/**
 * 导入(成绩　)
 */
@RestController
public class ImportController {
	@Autowired
	private ScoreService scoreService;

	@PostMapping("/save")
	public Map<String, Object> importTeam(@RequestParam("file") MultipartFile file) {
		Map<String, Object> map = new HashMap<String, Object>();
		String fileName = file.getOriginalFilename();
		try {
			map.put("code", "success");
			map.put("message", scoreService.batchImport(fileName, file));
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", "fail");
			map.put("message", "文件异常,导入失败");
		}
		return map;
	}

	/**
	 * 导入成绩
	 * @param file
	 * @return
	 */
	@PostMapping("/importScore")
	public Map<String, Object> importScore(@RequestParam("file") MultipartFile file) {
		Map<String, Object> map = new HashMap<String, Object>();
		String fileName = file.getOriginalFilename();
		try {
			map.put("code", "success");
			map.put("message", scoreService.importScore(fileName, file));
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", "falie");
			map.put("message", "文件异常,导入失败");
		}
		return map;
	}
}
