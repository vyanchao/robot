package com.star.robot.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.star.robot.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.star.robot.constant.Constant;
import com.star.robot.entity.Admin;
import com.star.robot.entity.Company;
import com.star.robot.entity.Project;
import com.star.robot.entity.TeamLeader;
import com.star.robot.entity.TeamMember;
import com.star.robot.entity.TeamScore;
import com.star.robot.enums.AdminTypeEnum;
import com.star.robot.enums.ProjectLevelEnum;
import com.star.robot.service.AdminService;

@Controller
public class PageController {
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired 
	private TeamLeaderRepository teamLeaderRepository;
	
	@Autowired
    private TeamScoreRepository teamScoreRepository;
	
	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	TeamMemberRepository teamMemberRepository;


	@Autowired
	AdminRepository adminRepository;
	
	/**
	 * 登陆页
	 * @param model
	 * @return
	 */
	@RequestMapping("/login")
	public String login(Map<String, Object> model) {
		return "/home/login";
	}
	
	/**
	 * 登出页
	 * @param model
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, Map<String, Object> model) {
		request.getSession().invalidate();
		return "/home/login";
	}
	
	/**
	 * 主页
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "/", "/index" })
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		//获取session  		
        Admin admin = (Admin) request.getSession().getAttribute(Constant.CURRENTADMIN);
		mav.addObject("admin", admin);
		mav.setViewName("/home/index");
		return mav;
	}
	

	/**
	 * 
	 * 修改密码页
	 */
	@RequestMapping("password")
	public ModelAndView password() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/home/password");
		return modelAndView;
	}
	


	
	
	/**
	 * 首页
	 * @param model
	 * @return
	 */
	@RequestMapping("/console")
	public ModelAndView console(HttpServletRequest request,Map<String, Object> model) {

		ModelAndView mav = new ModelAndView();
		//获取session
		Admin admin = (Admin) request.getSession().getAttribute(Constant.CURRENTADMIN);
		String areaCode = admin.getAdminTypeEnum().getCode().toString();

		if (AdminTypeEnum.PROVINCEADMIN.equals(admin.getAdminTypeEnum())) {
			areaCode = "41";
		}

		mav.addObject("schoolCount", companyRepository.countByType("0",areaCode + "%"));
		mav.addObject("jigouCount", companyRepository.countByType("1",areaCode + "%"));
		mav.addObject("teamCount", teamRepository.count());
		mav.addObject("memberCount", teamMemberRepository.count());


//		AdminIndexDto adminIndexDto = AdminIndexDto.builder()
//				.cpmpanyCount(companyRepository.count())
//				.schoolCount(companyRepository.countByType("0",areaCode + "%"))
//				.jigouCount(companyRepository.countByType("1",areaCode + "%"))
//				.teamCount(teamRepository.count())
//				.memberCount(teamMemberRepository.count())
//				.build();
//
//		System.out.println(adminIndexDto);

		mav.setViewName("/home/console");
		return mav;
	}

	/**
	 * 管理员页
	 * @return
	 */
	@RequestMapping("/teamManagement")
	public ModelAndView teamManagement() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/admin/list");
		return mav;
	}
	
	/**
	 * 添加管理员页
	 * @return
	 */
	@RequestMapping("/addTeamInfo")
	public ModelAndView addTeamInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/admin/add");
		return mav;
	}

	/**
	 * 添加管理员页
	 * @return
	 */
	@RequestMapping("/editTeamInfo")
	public ModelAndView editTeamInfo(long id) {
		ModelAndView mav = new ModelAndView();
		Admin  admin = adminRepository.findLongById(id);
		if(admin.getPhone() == null){
			admin.setPhone("");
		}
		mav.addObject("admin",admin);
		mav.setViewName("/admin/edit");
		return mav;
	}

	/**
	 * 单位页
	 * @return
	 */
	@RequestMapping("/companyList")
	public ModelAndView companyList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/company/companyList");
		return mav;
	}
	
	/**
	 * 添加单位页
	 * @return
	 */
	@RequestMapping("/addCompanyInfo")
	public ModelAndView addCompanyInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/company/addCompanyInfo");
		return mav;
	}
	/**
	 * 添加市级项目
	 * @return
	 */
	@RequestMapping("/addCityProject")
	public ModelAndView addCityProject() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/project/addCityProject");
		return mav;
	}
	
	/**
	 *修改单位页
	 * @return
	 */
	@RequestMapping("/editCompanyInfo")
	public ModelAndView editCompanyInfo(String id) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/company/editCompanyInfo");
		Company company = companyRepository.findLongById(Long.valueOf(id));
		//company.setTeams(null);
		mav.addObject("company", company);
		return mav;
	}
	
	/**
	 * 项目页
	 * @return
	 */
	@RequestMapping("/projectList")
	public ModelAndView projectList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/project/projectList");
		return mav;
	}
	
	/**
	 * 添加省级项目
	 * @return
	 */
	@RequestMapping("/addProvinceProjectInfo")
	public ModelAndView addProvinceProjectInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/project/addProvinceProjectInfo");
		return mav;
	}
	
	/**
	 * 添加市级项目
	 * @return
	 */
	@RequestMapping("/addCityProjectInfo")
	public ModelAndView addCityProjectInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/project/addCityProjectInfo");
		List<Project> list = projectRepository.findByProjectLevelEnum(ProjectLevelEnum.PROVINCELEVEL);
		mav.addObject("projectList", list);
		return mav;
	}
	
	/**
	 * 缴费管理页
	 * @return
	 */
	@RequestMapping("/payInfoList")
	public ModelAndView payInfoList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/info/payInfoList");
		return mav;
	}
	
	/**
	 * 报名项目页
	 * @return
	 */
	@RequestMapping("/signUpInfoList")
	public ModelAndView signUpInfoList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/info/signUpInfoList");
		return mav;
	}
	
	/**
	 * 队员管理页
	 * @return
	 */
	@RequestMapping("/editInfo")
	public ModelAndView editInfo(String id) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/teamMember/editInfo");
		TeamMember teamMember = teamMemberRepository.getOne(Long.valueOf(id));
		mav.addObject("teamMember", teamMember);
		return mav;
	}
	
	/**
	 * 修改队员页
	 * @return
	 */
	@RequestMapping("/teamManage")
	public ModelAndView teamManage(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/teamMember/list");
		Admin admin = (Admin) request.getSession().getAttribute(Constant.CURRENTADMIN);
		List<Project> projectList = projectRepository.findAll();
		mav.addObject("projectList", projectList);
		mav.addObject("admin",admin);
		return mav;
	}
	
	/**
	 * 队伍管理页
	 * @return
	 */
	@RequestMapping("/ranksManage")
	public ModelAndView ranksManage(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/team/list");
		List<Project> projectList = projectRepository.findAll();
		Admin admin = (Admin) request.getSession().getAttribute(Constant.CURRENTADMIN);
		mav.addObject("projectList", projectList);
		mav.addObject("admin", admin);
		return mav;
	}
	
	/**
	 * 弹框列表
	 */
	@RequestMapping("/teamform")
	public ModelAndView teamform(String teamId) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/team/teamform");
		List<TeamLeader> teamLeaders =  teamLeaderRepository.getByTeamId(Long.valueOf(teamId));
		if(!CollectionUtils.isEmpty(teamLeaders)){
			for(TeamLeader teamLeader : teamLeaders){
				teamLeader.setTeam(null);
			}
		}
		mav.addObject("teamLeaders", teamLeaders);
		return mav;
	}
	
	/**
	 * 弹框列表3
	 */
	@RequestMapping("/teammemberform")
	public ModelAndView teammemberform(String teamId) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/team/teammemberform");
		List<TeamMember> teamMembers =  teamMemberRepository.getByTeamId(Long.valueOf(teamId));
		if(!CollectionUtils.isEmpty(teamMembers)){
			for(TeamMember teamMember :teamMembers ){
				teamMember.setTeam(null);
			}
		}
		mav.addObject("teamMembers", teamMembers);
		return mav;
	}
	
	/**
	 * 弹框列表2
	 */
	@RequestMapping("/resultform")
	public ModelAndView resultform(String teamId) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/teamScore/resultform");
		List<TeamScore> teamScores =  teamScoreRepository.findByTeam_Id(Long.valueOf(teamId));
		mav.addObject("teamScores", teamScores);
		return mav;
	}
	
	
	/**
	 * 成绩管理页（省）
	 * @return
	 */
	@RequestMapping("/scoreProvinceManage")
	public ModelAndView scoreProvinceManage() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/teamScore/province");
		List<Project> projectList = projectRepository.findAll();
		mav.addObject("projectList", projectList);
		return mav;
	}
	
	/**
	 * 成绩管理页（市）
	 * @return
	 */
	@RequestMapping("/scoreCityManage")
	public ModelAndView scoreCityManage(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/teamScore/city");
		List<Project> projectList = projectRepository.findAll();
		Admin admin = (Admin) request.getSession().getAttribute(Constant.CURRENTADMIN);
		mav.addObject("projectList", projectList);
		mav.addObject("admin", admin);
		return mav;
	}
	
	/**
	 * 导出页
	 * @return
	 */
	@RequestMapping("/listform")
	public ModelAndView listform() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/listform");
		return mav;
	}
	
	/**
	 * 导出页
	 * @return
	 */
	@RequestMapping("/listformT")
	public ModelAndView listformT() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/listform2");
		return mav;
	}
	
	/**
	 * 上传页
	 * @return
	 */
	@RequestMapping("/uploadFile")
	public ModelAndView uploadFile() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/uploadFile");
		return mav;
	}
}
