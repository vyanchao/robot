package com.star.robot.enums;

public enum  MemberSexEnum {

    MALE(0,"男") , FEMALE(1,"女");
    private Integer code;

    private String name;

    MemberSexEnum(Integer code ,String name){
        this.code = code;
        this.name = name;
    }
}
