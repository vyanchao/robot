package com.star.robot.enums;

import com.star.robot.entity.Project;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "project_class_enum")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public enum ProjectClassEnum {
    XIAOXUE(0,"小学组") , CHUZHONG(1,"初中组"),GAOZHONG(2,"高中组");
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer code;

    private String name;



    ProjectClassEnum(Integer code, String name) {

        this.code=code;
        this.name=name;
    }

    @ManyToOne
    @JoinColumn(name = "project_id",referencedColumnName = "id",columnDefinition = "int(10) null COMMENT \"项目id\"")
    private Project project;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}

