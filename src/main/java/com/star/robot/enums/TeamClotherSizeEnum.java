package com.star.robot.enums;

public enum TeamClotherSizeEnum {
    S("S","S（140-155）") , M("M","M（155-170）"),L("L","L()")
    ,XL("XL","XL（170-185）"),XXXL("XXXL","XXXL（185以上）");
    private String code;

    private String name;

    TeamClotherSizeEnum(String code ,String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
