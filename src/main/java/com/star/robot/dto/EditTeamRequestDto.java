package com.star.robot.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.star.robot.entity.TeamLeader;
import com.star.robot.entity.TeamMember;
import com.star.robot.enums.ProjectClassEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by mawenlong on 2019/5/26.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditTeamRequestDto {
    private Long teamId;
    private List<TeamMember> members;
    private List<TeamLeader> leaders;
    private ProjectClassEnum projectClassEnum;
    private Long project_id;

    private String teamName;//队伍名称
}
