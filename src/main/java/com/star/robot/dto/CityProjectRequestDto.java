package com.star.robot.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.star.robot.enums.ProjectClassEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by mawenlong on 2019/5/26.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CityProjectRequestDto {
    private long Id;
    private long parentId;
    private List<ProjectClassEnum> projectClassEnum;
    private Boolean status;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date baoMingStartTime;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date baoMingEndTime;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date projectEndTime;
    private String descr;
}
