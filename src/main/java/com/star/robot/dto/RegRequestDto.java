package com.star.robot.dto;

import com.star.robot.entity.User;
import com.star.robot.enums.CompanyTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RegRequestDto extends User {
    private String verfiCode;//验证码

    private String newPass;//新密码

    private String companyName;

    private CompanyTypeEnum companyType;//分类 学校/机构

}
