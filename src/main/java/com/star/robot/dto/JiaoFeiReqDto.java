package com.star.robot.dto;

import lombok.Builder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JiaoFeiReqDto extends TeamBackendQueryDto {

    private Boolean status;
    private String startTime;
    private String endTime;
}
