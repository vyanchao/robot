package com.star.robot.dto;

import com.star.robot.entity.Team;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JiaoFeiDto extends TeamRespDto {
    private long id;

    private Date createTime;

    private Float value;//缴费金额

    private Team team;

    private String area;

    private Boolean status;



}
