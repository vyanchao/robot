package com.star.robot.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdminIndexDto {

    private Long cpmpanyCount;
    private Long jigouCount;
    private Long schoolCount;

    private Long teamCount;
    private Long memberCount;

}
