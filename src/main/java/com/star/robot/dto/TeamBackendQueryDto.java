package com.star.robot.dto;
import com.star.robot.enums.AdminTypeEnum;
import com.star.robot.enums.TeamClotherSizeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TeamBackendQueryDto extends PageRequestDto{

    private Long provinceId;

    private Long cityId;

    private Long areaId;

    private String teamMemberName; //队员姓名

    private String teamMemberIdCard;//队员身份证

    private TeamClotherSizeEnum clotherSizeEnum;

    private Long companyId;//单位ID

    private String teamName;//队伍名称

    private Integer companyType;//单位性质 1学校　２　机构

    //时间格式　2019-01-03 2019-01-04
    private String baoMingDateStart;

    private String baoMingDateEnd;


    private Integer groupType; //组别 1小学组　2中学组　3大学组

    private String companyName;

    private String projectName;

    private Long projectId;

    private Double scoreStart;

    private Double scoreEnd;

    private AdminTypeEnum adminTypeEnum;
}
