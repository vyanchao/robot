package com.star.robot.specialficaition;

import com.star.robot.dto.TeamBackendQueryDto;
import com.star.robot.entity.Team;
import com.star.robot.enums.AdminTypeEnum;
import com.star.robot.enums.CompanyTypeEnum;
import com.star.robot.enums.ProjectClassEnum;
import com.star.robot.enums.ProjectLevelEnum;
import com.star.robot.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Component
public class TeamSpecial {

    @Autowired
    private TeamRepository teamRepository;
    public Page<Team> findByRequestDto(Pageable page , TeamBackendQueryDto requestDto){
        Page<Team> pageable = teamRepository.findAll(new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery cq, CriteriaBuilder cb) {
                List<Predicate> conditions = new ArrayList<>();

                if(requestDto.getAdminTypeEnum() != null){
                    if(requestDto.getAdminTypeEnum() == AdminTypeEnum.PROVINCEADMIN){
                        Predicate adminTypeCond = cb.equal(root.get("team").get("project").get("projectLevelEnum") , ProjectLevelEnum.PROVINCELEVEL);
                        conditions.add(adminTypeCond);
                    }else{
                        Predicate adminTypeCond = cb.equal(root.get("team").get("project").get("projectLevelEnum") , ProjectLevelEnum.PROVINCELEVEL);
                        conditions.add(adminTypeCond);

                        //市

                        if(requestDto.getCityId() != null){
                            Predicate cityCondition = cb.equal(root.get("team").get("company").get("cityId")  , requestDto.getCityId());
                            conditions.add(cityCondition);
                        }
                    }
                }


                //单位名称
                if(!StringUtils.isEmpty(requestDto.getCompanyName())){
                    Predicate companyCond = cb.like(root.get("company").get("name") , "%"+requestDto.getCompanyName()+"%");
                    conditions.add(companyCond);
                }

                //省
//                    if(requestDto.getProvinceId() != null){
//                        Predicate provinceCondition = cb.equal(root.get("provinceId")  , requestDto.getProvinceId());
//                        conditions.add(provinceCondition);
//                    }
                //市
                if(requestDto.getCityId() != null){
                    Predicate cityCondition = cb.equal(root.get("company").get("cityId")  , requestDto.getCityId());
                    conditions.add(cityCondition);
                }
                //区
//                    if(requestDto.getAreaId() != null){
//                        Predicate areaCondition = cb.equal(root.get("areaId")  , requestDto.getAreaId());
//                        conditions.add(areaCondition);
//                    }
                //队员姓名
                if(!StringUtils.isEmpty(requestDto.getTeamMemberName())){
                    Predicate memberNameCond = cb.like(root.get("teamMembers").get("name")  , "%"+requestDto.getTeamMemberName()+"%");
                    conditions.add(memberNameCond);
                }
                //队员身份证
                if(!StringUtils.isEmpty(requestDto.getTeamMemberIdCard())){
                    Predicate memberIdCardCond = cb.like(root.get("teamMembers").get("idCard")  , "%"+requestDto.getTeamMemberName()+"%");
                    conditions.add(memberIdCardCond);
                }
                //队员衣服尺码
                if(!StringUtils.isEmpty(requestDto.getTeamMemberIdCard())){
                    Predicate memberIdCardCond = cb.equal(root.get("teamMembers").get("clotherSizeEnum")  , requestDto.getClotherSizeEnum());
                    conditions.add(memberIdCardCond);
                }

                //队伍名称
                if(!StringUtils.isEmpty(requestDto.getTeamName())){
                    Predicate teamNameCond = cb.like(root.get("teamName") , "%"+requestDto.getTeamName()+"%");
                    conditions.add(teamNameCond);
                }
                //单位性质
                if(requestDto.getCompanyType() !=null){
                    CompanyTypeEnum companyType = null;
                    if(requestDto.getCompanyType().intValue() == 1){
                        companyType =  CompanyTypeEnum.XUEXIAO;
                    }else if(requestDto.getCompanyType().intValue() == 2){
                        companyType =  CompanyTypeEnum.JIGOU;
                    }else{
                        //默认学校
                        companyType =  CompanyTypeEnum.XUEXIAO;
                    }
                    Predicate companyTypeCond = cb.equal(root.get("company").get("companyType") ,companyType);
                    conditions.add(companyTypeCond);

                }
//                    //项目名称
//                    if(!StringUtils.isEmpty(requestDto.getProjectName())){
//                        Predicate baoMingDateCond = cb.like(root.get("project").get("name") ,"%"+requestDto.getProjectName() + "%");
//                        conditions.add(baoMingDateCond);
//                    }
                //项目id
                if(requestDto.getProjectId() != null){
                    Predicate projectCond = cb.equal(root.get("project").get("id") , requestDto.getProjectId());
                    conditions.add(projectCond);
                }

                //报名时间 => 队伍　新增时间
                if(!StringUtils.isEmpty(requestDto.getBaoMingDateStart())
                        && !StringUtils.isEmpty(requestDto.getBaoMingDateEnd())){
                    Predicate baoMingDateCond = cb.between(root.get("createTime") ,requestDto.getBaoMingDateStart() , requestDto.getBaoMingDateEnd());
                    conditions.add(baoMingDateCond);
                }

                //组别
                if(requestDto.getGroupType() != null){
                    ProjectClassEnum groupTypeEnum = null;
                    if(requestDto.getGroupType() == 1){
                        groupTypeEnum = ProjectClassEnum.XIAOXUE;
                    }else if(requestDto.getGroupType() == 2){
                        groupTypeEnum = ProjectClassEnum.CHUZHONG;
                    }else if(requestDto.getGroupType() == 3){
                        groupTypeEnum = ProjectClassEnum.GAOZHONG;
                    }else{
                        //默认小学组
                        groupTypeEnum = ProjectClassEnum.XIAOXUE;
                    }
                    Predicate groupTypeCond = cb.equal(root.get("projectClassEnum") ,groupTypeEnum);
                    conditions.add(groupTypeCond);
                }

                Predicate[] pre = new Predicate[conditions.size()];

                cq.where(conditions.toArray(pre));
                return cq.getRestriction();
            }
        } , page);
        return pageable;
    }
}
